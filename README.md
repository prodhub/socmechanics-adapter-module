#SocMechanicsAdapter

Module SocMechanicsAdapter required for bind socmechanics items.

It works directly with SocMechanicsFactory to manage for data streams on client side

## Install

```sh
$ npm install --save @adwatch/socmechanics-adapter
```


## Usage

```js
import SocMechanicsAdapter from '@adwatch/socmechanics-adapter';
// or
var SocMechanicsAdapter = require('@adwatch/form/socmechanics-adapter');
```


## API


####init()

Initialize SocMechanicsAdapter

```js
const socMechanicsAdapter = new SocMechanicsAdapter(options).init();
```



## OPTIONS


When you create new exemplar SocMechanicsAdapter module you must to provide required argument **options** with options.

The Kit of options depends specific case. But module has common options which will be indicated separately

On current moment exists CASEs:
- ' freeShare:onePost:allNetworks ' - (only: vk.com, facebook.com, ok.ru)
- ' getAvatar:allNetworks ' - (only: vk.com, facebook.com, ok.ru, instagram.com, twitter.com)


###Common options

#####CASE
Type `string` (Required)

Default: `''`

```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    CASE: 'freeShare:onePost:allNetworks'
});
```


#####target
Type `string` (Required)

Default: `''`

```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    target: '.social button'
});
```


#####logger
Type `boolean`

Default: `'false'`

```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    logger: true
});
```



#####networks
Type `object` (Required)

Default: `'{}'`

Authenticated data your apps
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    networks: {
        'VK': {
            appId: XXXXXX
        },
        'FB': {
            appId: XXXXXX
        },
        'OK': {
            appId: XXXXXX,
            app_key: 'YYYYYY'
        },
        'TW': {
            proxy: '/tw.php'
        },
        'IN': {
            appId: 'XXXXXX'
        }
    }
});
```


#####action
Type `string`

Default: `'/'`

A path to send summary result of module`s job
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    action: '/share'
});
```


#####callSuccessSend
Type `function`

Default: `false`

A callback after success send summary data about user to your server
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callSuccessSend: function(data, serverRes){
        console.log('Your data received server', data, serverRes);
    }
});
```


#####callErrorServer
Type `function`

Default: `false`

A callback by server error
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callErrorServer: function(){
        console.log('Something wrong!');
    }
});
```


#####callBlockedModal
Type `function`

Default: `false`

A callback when user blocked modal window in his browser
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callBlockedModal: function(){
        console.log('You need to enable modal windows');
    }
});
```


###Options for ' freeShare:onePost:allNetworks '



#####timeShareLimit
Type `number`

Default: `4`

Share limit time in seconds
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    timeShareLimit: 4
});
```


#####infoShare
Type `object`

Default: `{}`

You can to see most common information to  https://www.npmjs.com/package/@adwatch/share
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    infoShare: {
       description: 'Test page desc',
       img: 'http://example.ru/img/share.jpg',
       title: 'Test Share',
       url: window.location.href,
       workId: 2
   }
});
```


#####token
Type `object`

Default: `{}`

User Token
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    token: 'XXX'
});
```


#####callBadShareClient
Type `function`

Default: `false`

Callback after Client Bad Share
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callBadShareClient: function(){
        console.log('Client Bad Share!!!');
    }
});
```


#####callErrorSend
Type `function`

Default: `false`

Callback after Backend Bad Share
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callErrorSend: function(result){
        console.log('Backend Bad Share!!!', result);
    }
});
```


#####callClientAllow
Type `function`

Default: `false`

```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callClientAllow: function(){
        console.log('You must approve conditions of use application');
    }
});
```


#####callPrivateSettings
Type `function`

Default: `false`

Required by Facebook
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callPrivateSettings: function(){
        console.log('You must approve conditions of use application');
    }
});
```


#####callShareStart
Type `function`

Default: `false`

Callback when share start
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callShareStart: function(elem){
        elem.disabled = true;
        elem.classList.add('blocked');
    }
});
```


#####callShareEnd
Type `function`

Default: `false`

Callback when share ends
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callShareEnd: function(){
       console.log('Share ends');
    }
});
```


#####callTimeStamp
Type `function`

Default: `false`

Callback when share ends
```js
let socMechanicsAdapter = new SocMechanicsAdapter({
    callTimeStamp: function(elem, timer, int, timeLimit){
       var status = elem.closest('.social').querySelector('.status');

        if(timer > timeLimit){
            clearInterval(int);
            elem.disabled = false;
            elem.classList.remove('blocked');
            status.innerHTML = '';
        }else{
            status.innerHTML = 'Will be available after ' + (timeLimit - timer);
        }
    }
});
```

 ## License

 MIT ©
