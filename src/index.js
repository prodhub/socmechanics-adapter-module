'use strict';

var Share = require('@adwatch/share/build');
var axios = require('axios');
var SocMechanicsFactory = require('@adwatch/socmechanics-factory/build').SocMechanicsFactory;


const _protect = {
    addListener: (el, event, handler)=> {
        if(el.addEventListener){
            el.addEventListener(event, handler, false);
        }else if(el.attachEvent){
            el.attachEvent(`on${event}`, handler);
        }
    },
    removeListener: (el, event, handler)=>{
        if(el.removeEventListener){
            el.removeEventListener(event, handler, false);
        }else if(el.detachEvent){
            el.detachEvent('on' + event, handler);
        }
    },
    bindEvent: function(){
        let that = this,
            targets = document.querySelectorAll(that.target);
        _protect.listeners = [];

        [].forEach.call(targets,(target)=>{
            _protect.listeners.push(target);
            _protect.addListener(target, 'click', _protect.clickEvent);
        });
    },
    clickEvent: function(e){
        e.preventDefault();

        let elem = e.target,
            { network, work } = elem.dataset;

        if(_protect.factoryLink && _protect.caseLink){
            _protect.factoryLink.controller(_protect.caseLink, network, work, elem);
        }
    },
    cases: {
        'freeShare:onePost:allNetworks': true,
        'getAvatar:allNetworks': true
    },
    socMechanicsFactory: new SocMechanicsFactory,
    logger: function(step, results){
        console.info(step, results);
    },
    getUserInfo: function(that, share, CASE, elem, network, work){
        //Get user info
        let promiseStatus = that.getUserInfo(network);
        promiseStatus.then(
            result => {
                share = null;

                that.logger && _protect.logger('GET_USER_INFO', result);

                //Get Post info
                let promisePost = that.getUserPost(network);
                promisePost.then(
                    result => {

                        that.logger && _protect.logger('GET_USER_POST', result);

                        var post = result;
                        let timeStampPost = that[network].getLastPost('timeStamp');
                        let timeDiff = (Math.ceil(that.timeShareStart/1000)) - timeStampPost;

                        if(timeDiff > that.timeShareLimit){
                            that.callBadShareClient && that.callBadShareClient();
                            throw new TypeError('Time limit error');
                        }


                        //Check last Public Post on the user wall
                        if(that[network].checkWallPost(that.infoShare)){
                            //Send Log
                            let promiseSendLog = that.sendUserInfo(network, CASE, {work});
                            promiseSendLog.then(
                                result => {

                                    that.logger && _protect.logger('GET_RESULT', result);

                                    if(result.status.toString() === 'true'){
                                        //Good Log result
                                        that.callSuccessSend && that.callSuccessSend(post, result);
                                    }else{
                                        //Bad Log result
                                        that.callErrorSend && that.callErrorSend(result);
                                    }
                                }
                            ).catch(e => {console.log(e)});
                        }else{
                            //Client Bad share
                            that.callBadShareClient && that.callBadShareClient();
                            throw new TypeError('Client share error');
                        }
                    }
                ).catch(e => {if(e === 'PrivateSettingsError'){
                    that.callPrivateSettings && that.callPrivateSettings();
                }});
            }
        ).catch(e => {console.log(e)});

        that.timeShareEnd[network] = new Date().getTime();
        let stampInt = setInterval(()=>{
            that.callTimeStamp && that.callTimeStamp(elem, (Math.round((new Date().getTime() - that.timeShareEnd[network])/1000)), stampInt, that.timeShareLimit);
        }, 1000);
        that.currentNetwork = network;
        that.callShareEnd && that.callShareEnd(elem);
    }
};

class SocMechanicsAdapter{

    constructor(config){
        //Required params
        if(typeof config.target === 'string'){
            this.target = config.target;
        }else{
            throw TypeError('Don`t set target');
        }
        if(typeof config.CASE === 'string'){
            if(_protect.cases[config.CASE]){
                this.CASE = config.CASE;
            }else{
                throw TypeError(`Not exist case ${config.CASE}`);
            }
        }else{
            throw TypeError('Don`t set CASE');
        }

        //Share config
        this.callBadShareClient = (typeof config.callBadShareClient === 'function') ? config.callBadShareClient : false;

        //Callbacks
        this.infoShare = (typeof config.infoShare === 'object') ? config.infoShare : null;
        this.callErrorSend = (typeof config.callErrorSend === 'function') ? config.callErrorSend : false;
        this.callSuccessSend = (typeof config.callSuccessSend === 'function') ? config.callSuccessSend : false;
        this.callErrorServer = (typeof config.callErrorServer === 'function') ? config.callErrorServer : false;
        this.callClientAllow = (typeof config.callClientAllow === 'function') ? config.callClientAllow : false;
        this.callBlockedModal = (typeof config.callBlockedModal === 'function') ? config.callBlockedModal : false;
        this.callPrivateSettings = (typeof config.callPrivateSettings === 'function') ? config.callPrivateSettings : false;
        this.callShareStart = (typeof config.callShareStart === 'function') ? config.callShareStart : false;
        this.callShareEnd = (typeof config.callShareEnd === 'function') ? config.callShareEnd : false;
        this.callTimeStamp = (typeof config.callTimeStamp === 'function') ? config.callTimeStamp : false;
        //Options
        this.logger = (typeof config.logger === 'boolean') ? config.logger : false;
        //Networks
        this.networks = Object.assign({}, config.networks);
        //Url
        this.action = config.action || '/';
        this.token = config.token;
        //Share timer
        this.timeShareStart = 5;
        this.timeShareEnd = {};
        this.timeShareLimit = config.timeShareLimit || 4;

    }

    unbind(targetNetwork){
        if(targetNetwork){
            _protect.listeners.filter((elem)=>{
                let {network} = elem.dataset;
                if(targetNetwork.toUpperCase() == network){_protect.removeListener(elem, 'click', _protect.clickEvent)}
            });
        }else{
            _protect.listeners.forEach((elem)=>{
                _protect.removeListener(elem, 'click', _protect.clickEvent);
            });
        }
    }

    sendUserInfo(network, _case, params){
        let that = this;
        var pack = {};

        switch(_case){
            case 'freeShare:onePost:allNetworks':{
                pack = that[network].makePackage(_case);
                pack.token = that.token;
                pack.workId = params.work;

                break;
            }
            case 'getAvatar:allNetworks':{
                pack = that[network].makePackage(_case);

                break;
            }
            default:
                pack = {method: 'Not Allow Method'};
                break;
        }

        that.logger && _protect.logger('MAKE_PACKAGE', pack);

        return new Promise((resolve, reject)=>{
            axios.post(that.action, {data: pack})
                .then((res)=>{
                    if(res.status == 200){
                        resolve(res.data);
                    }
                })
                .catch((err)=>{
                    if(that.callErrorServer){that.callErrorServer()}
                    reject(err);
                });
        });
    }

    getUserAuth(network){
        let promise = this[network].getUserAuth();
        return promise;
    }

    getUserInfo(network){
        let promise = this[network].getUserInfo();
        return promise;
    }

    getUserPost(network){
        let promise = this[network].getUserPost();
        return promise;
    }

    getLastPost(network){
        return this[network].getLastPost();
    }

    getUserAvatar(network){
        if(network !== 'OK'){
            return this[network].getUserAvatar();
        }else{
            return this[network].getUserInfo();
        }
    }

    controller(CASE, network, work, elem){
        let that = this;

        switch (CASE){
            case 'freeShare:onePost:allNetworks':
                //--------------------------------------------------

                if(Math.round((new Date().getTime() - that.timeShareEnd[network])/1000) < that.timeShareLimit && that.currentNetwork == network){return false;}

                let share = new Share({
                    callbackPopupShareClosed: ()=>{

                        that.callShareStart && that.callShareStart(elem);

                        that.timeShareStart = new Date().getTime();

                        _protect.getUserInfo(that, share, CASE, elem, network, work)
                    },
                    callbackError: that.callBlockedModal
                });

                //Check auth user (+ incognito mode)
                let promiseAuth = that.getUserAuth(network);
                promiseAuth.then(
                    result => {

                        that.logger && _protect.logger('AUTHORIZATION', result);

                        let paramsPackage = Object.assign({}, that.infoShare, {type: network.toLowerCase()});

                        if(network !== 'FB'){
                            share.share(network.toLowerCase(), paramsPackage);
                        }else{
                            FB.ui({
                                method: 'feed',
                                link: that.infoShare.url,
                                description: that.infoShare.description,
                                picture: that.infoShare.img,
                                name: that.infoShare.title
                            }, function(response){
                                _protect.getUserInfo(that, share, CASE, elem, network, work);
                            });
                        }

                    }
                ).catch(e => {that.callClientAllow && that.callClientAllow()});
                //--------------------------------------------------
                break;

            case 'getAvatar:allNetworks': {
                let promiseAuth = that.getUserAuth(network);
                promiseAuth.then(
                    result => {

                        that.logger && _protect.logger('AUTHORIZATION', result);

                        let promiseUser = that.getUserAvatar(network);
                        promiseUser.then(
                            result => {

                                that.logger && _protect.logger('GET_USER_AVATAR', result);

                                var avatar = result;
                                //Send User Photo Profile
                                let promiseSendAvatar = that.sendUserInfo(network, CASE);
                                promiseSendAvatar.then(
                                    result => {
                                        that.callSuccessSend && that.callSuccessSend(avatar, result);
                                        that.logger && _protect.logger('GET_RESULT', result);
                                    }
                                ).catch(e => {that.callErrorSend && that.callErrorSend(e)});
                            }
                        ).catch(e => {that.callClientAllow && that.callClientAllow(e)});
                    }
                ).catch(e => {that.callClientAllow && that.callClientAllow(e)});

                break;
            }

            default:
                break;
        }
    }

    eventRegister(){
        _protect.bindEvent.call(this);
        _protect.factoryLink = this;
        _protect.caseLink = this.CASE;
        return this;
    }

    init(){
        let that = this;

        if(document.readyState == 'loading'){
            document.addEventListener('DOMContentLoaded', () =>{
                initItems();
            });
        }else{
            initItems();
        }

        function initItems(){
            for(var network in that.networks){
                let config = that.networks[network];

                that[network] = _protect.socMechanicsFactory.produce(network, config);
                that[network].init();

                that.timeShareEnd[network] = 0;
            }

            delete that.networks;

            that.eventRegister();

            return that;
        }

        return this;

    }
}

window.SocMechanicsAdapter = SocMechanicsAdapter;

module.exports = SocMechanicsAdapter;